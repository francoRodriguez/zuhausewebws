<?php

namespace lib;

require "lib/configuracion.php";
require "lib/administradorUsuario/administradorUsuario.php";
require "lib/logs.php";

use zuHause;
use \lib\administradorUsuario\AdmUsuario;
use \lib\Configuracion;
use \lib\Logs;
use \lib\Mensajes;
use Exception;


class zuHauseWS{

    private $error                  = 0;
    private $ruta_configuracion     = 'etc/configuracion.ini';
    private $ruta_ambiente          = 'etc/ambiente.ini';
    private $ambiente               = 'desarrollo';
    private $configuracion          = null;
    private $log                    = null;

    # Constructor de la clase
    public function __construct(){
        try {
            $this->configuracion    = new Configuracion($this->ruta_configuracion, $this->ambiente, $this->ruta_ambiente);            
            $this->ambiente         = $this->configuracion->getAmbiente();                                    
            $this->usuario          = new AdmUsuario($this->ruta_configuracion, $this->ambiente);            
            //$this->log              = new Logs($this->ruta_configuracion, $this->ambiente);            
        } catch (Exception $e) {
            $mensaje_excepcion = Mensajes::getMensaje("001", array("funcion" => "__construct", "mensaje" => $e->getMessage()));
            error_log($mensaje_excepcion);    
            throw new Exception($e->getMessage(), (int)$e->getCode());        
        }
    }

        # Login de los usuarios al sistema
        public function ingresar($login = "", $clave = ""){
        	try{
        		
        		if($login == "" or $clave == ""){
        			$mensaje = Mensajes::getMensaje("041", array());
        			return json_encode(array("valido"=>0,"mensaje"=>$mensaje));
        		}
                $res = $this->usuario->loguear($login, $clave);                
                error_log(print_r($res,1));

        		$this->usuario->finalizar();
        		$mensaje = $res->mensaje;
                                
                return json_encode(array("valido"=>1,"mensaje"=>$mensaje));
                                
        	}catch(Exception $e){
        		$mensaje_excepcion = Mensajes::getMensaje("003", array("funcion"=>"ingresar","mensaje"=>$e->getMessage()));
                error_log($mensaje_excepcion);
                $mensaje = Mensajes::getMensaje("011", array());
            	return json_encode(array("valido"=>0,"mensaje"=>$mensaje));
            }
       }
        
        
       # Salida de los usuarios del sistema
       public function logout($args = array()){
       		//$this->session->persistir();
       		return  json_encode(array("error"=>0));
       }





}
