<?php 

    namespace lib;

    include_once 'includes.php';

    use lib\Mensajes;
    use Exception;

    class zuHause{

        // Busca todos los usuarios de la base de datos que cumplan con el $patron
        function habilitarSMS($cliente, $trabajo, $emision, $lote){
        	try {

                if($lote == 0){
                    // si es 0 modifico todos los lotes de la emision dada
                    $consulta = "";
                } else{
                    $consulta = "";
                }
        		// update los registros segun emision, lote
        		
                $res = $this->basedatos->ExecuteNonQuery($consulta, array($cliente, $trabajo, $emision, $lote));

                $cantidadDatos = count($res);                       		
        		//$this->finalizar ();
        		$mensaje = Mensajes::getMensaje ( "002", array ("cantidad" => $cantidadDatos) );
        		return array ("error" => 0,"mensaje" => $mensaje);
        	}catch ( Exception $e ) {
        		$mensaje_excepcion = Mensajes::getMensaje("006",array("funcion" => "buscarUsuarios","mensaje" => $e->getMessage ()));
        		$mensaje = Mensajes::getMensaje("011",array());
        		error_log($mensaje_excepcion);
        		$this->error = 1;
        		//$this->finalizar ();
        		return array ("error" => 1,"datos" => array(), "cantidad_datos" => 0,"mensaje" => $mensaje);
        	}
        }
    }

?>