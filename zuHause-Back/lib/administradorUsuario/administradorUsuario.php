<?php
 
	namespace lib\administradorUsuario;

	include "lib/DataBase.php";
	include "lib/administradorUsuario/Mensajes.php";

	//use \lib\nucleo\Mail;
	use \lib\administradorUsuario\Mensajes as MensajesUsuario;
	use \lib\Database;
	use \lib\Configuracion;	
	use stdClass;
	use Exception;
	use lib\Mensajes;

class AdmUsuario{
	
		var $configuracion 	= null;
		var $basedatos		= null;
		var $session		= null;
		var $error 			= 0;
		protected $mail;
		
		
		/**
		* Constructor del AdmUsuario
		*
		* El constructor del AdmUsuario setea parámetros necesarios para el funcionamiento,
		* crea las clases 'Configuracion' y 'DataBase', inicializa una transacción en la base
		* y obtiene desde el archivo de configuración la cantidad de claves anteriores que se
		* deben verificar que no se repitan al momento de cambiar un clave
		*  
		* @param string $ruta_configuracion ruta del archivo desde donde se cargan las configuraciones
		* @param string $ambiente ambiente en el que se está corriendo el AdmUsuario (desarrollo o producción)
		* 
		* @access public
		*/
		public function __construct($ruta_configuracion = "", $ambiente = ""){			
			try{
				$this->ruta_configuracion 	= $ruta_configuracion;
				$this->ambiente		 		= $ambiente;
				$this->configuracion 		= new Configuracion($ruta_configuracion, $ambiente);			
				$this->basedatos 	 		= new Database($ruta_configuracion, $ambiente);				
				$this->error				= 0;				
				$this->basedatos->BeginTransaction();				
				//$this->historial_password = $this->configuracion->getDato("cant_historial_password");				
			}
			catch(Exception $e){
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
			
		
		/**
		* Finalizar del AdmUsuario
		*
		* Se encarga de verificar si ocurrió algún error en la ejecución del AdmUsuario y si
		* ocurrió algún error le hace un rollback a la transacción activa de la base de datos
		* dejándola en un estado consistente, si no ocurrió ningún error commitea los cambios 
		* a la base de datos persistiendo de esta manera los datos.
		*
		* @access public
		*/
		public function finalizar(){ 
			# Se fija si ocurrió algún error
			if($this->error == 0){
				// Si no hubo error commitea
				$this->basedatos->CommitTransaction();
			}
			else{
				// Si hubo error hace rollback
				$this->basedatos->RollBackTransaction();
			}
		} 
		
		
		/**
		* Se encarga de loguear un usuario en el sistema
		*
		* Esta función se encarga de loguear un usuario en el sistema, para eso verifica los datos del usuario y en caso de que la autenticación del usuario sea exitosa retorna todos los datos del usuario. 
		* La secuencia de ejecución es la siguiente:
	    * 	1-	Se fija si el login o la contraseña son vacíos, en cuyo caso aborta la ejecución y retorna el mensaje “Debe introducir login y password”.
		*	2-	Obtiene el id del Usuario, y verifica si existe el login. 
		*	3-	Valida el usuario, usando la función “__validarUsuario”. 
		*	4-	Obtiene los datos generales del usuario.
		*	5-	Crea una nueva sesión para el usuario.
		*	6-	Guarda el nombre del usuario en la sesión.
		*	7-	Obtiene los grupos a los que el usuario puede crearle usuarios.
		*	8-	Obtiene las funciones del usuario.
		*	9-	Obtiene los grupos del usuario y los hijos de dichos grupos.
		*	10-	Crea una clase para retornar el resultado.
		*	11-	Obtiene los datos del perfil.
		*	12-	Retorna todos los datos obtenidos del usuario.
		*	13-	Si hay algún error en las operaciones de la secuencia, tira una excepción con el mensaje que corresponda.
		*
		* @param string $login login del usuario que intenta ingresar al sistema
		* @param string $pass contraseña del usuario
		* @param string $ip ip desde la cual se esta ingresando
		* 
		* @return array contiene los siguientes datos del usuario:
		*			valido: Indica si es válido el ingreso del usuario.
		*			gruposTodos: Grupos a los que pertenece el usuario, más sus hijos.
		*			grupos: Grupos a los que el usuario puede crearle usuarios	
		*			funciones: Las funciones que puede ejecutar el usuario.	
		*			perfil: Datos del perfil del usuario. 				
		*			resetear: Si hay que resetear la password. 
		*			sesión: una nueva sesión.
       	*			id: id del usuario en la tabla.
		*
		* @access public
		*/
		public function loguear($login = "", $pass = ""){
			try{
				# Me fijo si la contraseÃ±a o es el password son vacios
				if($login == "" or $pass == ""){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('000', array());
					throw new Exception($mensaje, '000');
				}
				# Obtiene el Id del usuario
				$idUsuario = $this->__getIdUsuario(strtolower($login)); // Obtiene el id del Usuario

				# Si no existe el usuario tira un error
				if($idUsuario==0){
					$mensaje = MensajesUsuario::getMensaje('001', array());
					$salida = new stdClass();
					$salida->valido = 0;
					$salida->mensaje = $mensaje;
					return $salida;
				}				

				$resetear = "";								
				# Obtengo los datos generales del usuario
				$resUsuario = $this->getUsuario(strtolower($login));

				error_log(print_r($resUsuario,1));
				
				$salida = new stdClass();
				
				$salida->valido   = 1;
				$salida->id       = $idUsuario;
				
				$mensaje = MensajesUsuario::getMensaje('002', array());
				$salida->mensaje = $mensaje;
				
				return $salida;
			}
			catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		
		/**
		* Modifica lo datos del usuario
		*
		* Cambia los atributos especificados del usuario, estos pueden ser:
		* activado(S/N), habilitado(S/N), externo(S/N) y/o resetear(S/N).
		* La secuencia de ejecución es la siguiente:
		*	1-	Verifica si el idUsuario es vacío, si es vacío tira error.
		*	2-	Verifica si el arreglo de parámetros a modificar es vacío, si es vacío tira error.
		*	3-	Se fija que atributos fueron pasados como parámetro y verifica que el formato sea válido, cambia las "S" por "1" y las "N" por "0" y si verifica lo agrega a la consulta.
		*	4-	Actualiza la base de datos con los valores pasados como parámetro.
		*
		* @param string $idUsuario id del usuario a modificar.
		* @param array $atributos hash que contiene pares (atributo, valor) a modificar.
		*
		* @access public
		**/
		public function modificarUsuario($idUsuario = "", $atributos = array()){
			# Verifica si el idUsuario es vacio, si es vacio tira error 
			if($idUsuario == ""){
				$this->error = 1;
				$mensaje = MensajesUsuario::getMensaje('003', array("PARAMETRO"=>'id usuario'));
				throw new Exception($mensaje, '003');
			}
			# Verifica si el arreglo de parametros a modificar es vacio, si es vacio tira error
			if(empty($atributos)){
				$this->error = 1;
				$mensaje = MensajesUsuario::getMensaje('003', array("PARAMETRO"=>'atributos'));
				throw new Exception($mensaje, '003');
			}
			try{
				#Crea la consulta vacia
				$cont_consulta = "";
				#Arreglo auxiliar de valores para la ejecucion de la consulta
				$array_valores = array();
				#Arreglo de posibles valores a modificar
				$camposSN = array("activado","habilitado","externo","resetear");
				# Se fija que atributos fueron pasados como parametro y verifica que el formato sea valido, cambia 
				# las "S" por "1" y las "N" por "0" y si verifica lo agrega a la consulta
				foreach ($camposSN as $k => $elem){
					$seteado = 0;
					if (isset($atributos[$elem])){
						#verifico formato, si no es valido retorno excepcion
						if ($atributos[$elem] == 'S'){
							$seteado = 1;
						}elseif ($atributos[$elem] == 'N'){
							$seteado = 0;
						}else {
							$mensaje = MensajesUsuario::getMensaje('003', array("PARAMETRO"=>$elem));
							throw new Exception($mensaje, '003');
						}
						#Agrego campo y valor a la consulta
						if($cont_consulta != ""){
							$cont_consulta .= ",";
						}
						$cont_consulta .= " $elem = ? ";
						$array_valores[] = $seteado;
					}
				}
				$cont_consulta = preg_replace('/,$/', '', $cont_consulta);
				$array_valores[] = $idUsuario;
				# Actualiza la base de datos con los valores pasados como parametro
				$consulta = " UPDATE usuario SET $cont_consulta  WHERE id = ? ";
				$res 		= $this->basedatos->ExecuteNonQuery($consulta, $array_valores, false);
			}
			catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}

		
		/**
		* Devuelve el id de un usuario
		*
		* Consulta la base de datos y retorna el identificador del usuario
        * La secuencia de ejecución es la siguiente:
		* 	1-	Verifica si el login pasado como parámetro es vacío, si es así retorna una excepción.
		*	2-	Hace la consulta a la base de datos para obtener el id del usuario desde la tabla usuario.
		*	3-	Si no existe el usuario tira una excepción.
		*	4-	Si el usuario existe retorna el id del usuario.
	    *
		* @param string $login: login del usuario 
		*
		* @return integer $id identificador del usuario
		*
		* @access private
		**/
		protected function __getIdUsuario($login = ""){
			try{
				# Verifica si el login pasado como parámetro es vacío, si es así retorna una excepción
				if($login == ""){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('003', array("PARAMETRO"=>'login'));
					throw new Exception($mensaje, '003');
				}else{
					# Hace la consulta a la base de datos para obtener el id del usuario desde la tabla usuario.
					$consulta = "SELECT id FROM usuario WHERE login = ? ";
					$res = $this->basedatos->ExecuteQuery($consulta, array(strtolower($login)));
					# Si no existe el usuario tira una excepción
					if(!isset($res[0]->id)){
						return 0;
					}else{
						# Si el usuario existe retorna el id del usuario
						return $res[0]->id;
					}
				}
			}
			catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		
		/**
		* Retorna los datos generales del usuario
		*
		* Busca en la tabla “usuarios” de la base de datos y retorna los datos generales del usuario pasado como parámetro.
		* La secuencia de ejecución es la siguiente:
		* 	1-	Obtiene el id del Usuario
		*	2-	Ejecuta la consulta para obtener los datos de la tabla usuario.
		*	3-	Modifica los datos obtenidos de la base sustituyendo los “1” por “S” y los “0” por “N”.
		*	4-	Retorna los datos obtenidos con la consulta. 
	    *
		* @param string $login: Usuario del cual se quieren obtener los datos generales.
		*
		* @return array Devuelve en un hash los datos generales de un usuario:	
		*			dia_alta, hora_alta, activado(S/N), resetear(S/N), habilitado(S/N), externo(S/N).
		*
		* @access public
		**/
		public function getUsuario($login = ""){
			try{
				$idUsuario = $this->__getIdUsuario(strtolower($login)); // Obtiene el id del Usuario

				if($idUsuario==0){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('004', array("USUARIO" => strtolower($login)));
					throw new Exception($mensaje, "004");
				}
				// Prepara la consulta para obtener los datos de la tabla usuario
				$consulta 	= 'SELECT nombre, apellido, telefono, dia_alta, habilitado, resetear, fecha_cambio_password, ultimo_acceso FROM usuario WHERE id = ?';
				$res 	  	= $this->basedatos->ExecuteQuery($consulta, array($idUsuario));
				$salida = $res[0];
				if($res[0]->resetear == 0){
					$salida->resetear = 'N';
				}
				else{
					$salida->resetear = 'S';
				}
				if($res[0]->habilitado == 0){
					$salida->habilitado = 'N';
				}
				else{
					$salida->habilitado = 'S';
				}
				return $salida;
			}
			catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
								
		/** 
		 * 	Crea un nuevo usuario en la base de datos.
		 *	La secuencia de ejecución es la siguiente:
		 *	1-	Si el login o la password son vacíos o no están definidos tira una excepción.
		 *	2-	Calcula el hash sha1 de la password.
		 *	3-	Verifica que el nombre de usuario sea único.
		 *	4-	Si ya existe el usuario tira una excepción.
		 *	5-	Verifica si está definido el parámetro de entrada activado, si lo esta se fija que tenga un valor válido (S/N), si lo tiene cambia los caracteres (S/N) por (1/0) y lo agrega a la consulta, sino tira una excepción.
		 *	6-	Verifica si está definido el parámetro de entrada habilitado, si lo esta se fija que tenga un valor válido (S/N), si lo tiene cambia los caracteres (S/N) por (1/0) y lo agrega a la consulta, sino tira una excepción
		 *	7-	Verifica si está definido el parámetro de entrada externo, si lo esta se fija que tenga un valor válido (S/N), si lo tiene cambia los caracteres (S/N) por (1/0) y lo agrega a la consulta, sino tira una excepción.
		 *	8-	Inserta el registro del usuario en la tabla usuario.
		 *	9-	Retorna el id del usuario creado.	
         *  
		 *  @param string $login: login del usuario
		 *  @param string $activado: indica si el usuario está activado
	     *  @param string $habilitado: indica si el usuario está habilitado para loguearse
	     *  @param string $externo: indica si es externo 
         *
		 *  @return integer Id del usuario creado 	
		 * 
		 *  @access private
		 **/
		protected function __crearUsuario($login = "", $password = "", $nombre = "", $apellido = "", $telefono = "", $habilitado = 'S'){
			try{
				# Si el login o la password son vacíos o no están definidos tira una excepción
				if ((!isset($password)) or (!isset($login)) or ($password == "") or ($login == "")){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('001', array('login'));
					throw new Exception($mensaje, '001');
				}
				$password = password_hash($password, PASSWORD_DEFAULT);
				# Verifica que el nombre de usuario sea unico
				$idUsuario = $this->__getIdUsuario(strtolower($login));
				if($idUsuario !=0 ){
					return 0;
				}
				$valores		= array();
				$valores[] 		= strtolower($login);
				$valores[] 		= $password;
				$valores[] 		= $nombre;
				$valores[] 		= $apellido;
				$valores[] 		= $telefono;
				$valores_		= "";
				$cont_consulta 	= "";

				# Verifica si está definido el parámetro de entrada habilitado, si lo esta se fija que tenga un valor válido (S/N),
				# si lo tiene cambia los caracteres (S/N) por (1/0) y lo agrega a la consulta, sino tira una excepción
				if (isset($habilitado)){
					if ($habilitado == 'S'){
						$habilitado	=	1;
					}elseif ($habilitado == 'N'){
						$habilitado	=	0;
					}else {
						$this->error = 1;
						$mensaje = MensajesUsuario::getMensaje('005', array());
						throw new Exception($mensaje, '005');						
					}
				}

				$parametros = array($login, $password, $nombre, $apellido, $telefono, $habilitado);
				# Inserta el registro del usuario en la tabla usuario
				$consulta =	"INSERT INTO usuario "."(login, password, nombre, apellido, telefono, dia_alta, habilitado, resetear, fecha_cambio_password, ultimo_acceso   )	"."VALUES (?, ?, ?, ?, ?, NOW(), ?, ?, NOW(), NOW())";	
				$idNew 	  = $this->basedatos->ExecuteNonQuery($consulta, $parametros, true);

				# Retorna el id del usuario creado
				return $idNew;
			}
			catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		/** 
		 * Crea el usuario
		 * La secuencia de ejecución es la siguiente:
		 * 1-	Crea el usuario en la base de datos, usando la función  “__crearUsuario”.
		 * 2-	Retorna el id del usuario creado.
		 *  
		 * @param string $login: login del usuario
		 * @param string $password: password del usuario
		 * @param string $nombre: nombre del usuario
		 * @param string $apellido: apellido del usuario
		 * @param string $telefono: telefono del usuario
         * @param string $habilitado: si el usuario esta habilitado o no por defecto lo creo en 1/S
		 * 
		 * @return integer Id del usuario creado 
		 * 
		 **/
		public function altaUsuario ($login = "", $password = "", $nombre = "", $apellido = "", $telefono = "", $habilitado = "S"){
			try{
				if ((!isset($login)) or ($login == "")){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('006', array());
					throw new Exception($mensaje, '006');
				}
				
				// Crea el usuario en la base de datos
				$idUsuario = $this->__crearUsuario(strtolower($login),$password,$nombre,$apellido,$telefono,$habilitado);
				if($idUsuario == 0){
					// Si el usuario ya existe retorna 0
					return 0;
				}
				// Retorna el id del usuario creado
				return $idUsuario;
			}
			catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		
		/** 
		* Elimina un usuario de la base de datos
		* 
		* La secuencia de ejecución es la siguiente:
		* 1- Controla que el usuario a liminar no sea el root.
		* 2- Obtiene el id del usuario a eliminar.
		* 3- Elimina el usuario.
		*
		* @param string $login: login del usuario
		* 
		**/
		public function eliminarUsuario($login = ""){
			try{
				// Controla que el usuario a eliminar no sea el root
				if(strtolower($login) == "root"){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('007', array());
					throw new Exception($mensaje, '007');
				}
				// Obtiene el id del usuario a eliminar
				$idUsuario = $this->__getIdUsuario(strtolower($login));
				if($idUsuario==0){
					$this->error = 1;
					$mensaje = Mensajes::getMensaje('001', array("USUARIO" => strtolower($login)));
					throw new Exception($mensaje, "001");
				}
				// Elimina el usuario
				$consulta = ' DELETE FROM usuario WHERE id = ? ';
				$res = $this->basedatos->ExecuteNonQuery($consulta, array($idUsuario));
			}catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		/** 
		 * Modifica el password de un usuario.
		 * 
		 * La secuencia de ejecución es la siguiente:
         * 1-	Se fija si los parámetros login, clave nueva y clave antigua no están definidos y o son vacíos, si es así tira una excepción.
         * 2-	Obtiene desde el archivo de configuración el formato que tiene que cumplir la clave.
         * 3-	Se fija si la nueva clave cumple el formato.
         * 4-	Calcula el SHA1 de la clave nueva y la clave vieja.
         * 5-	Verifica si existe el usuario con los parámetros login y clave pasados como parámetro.
         * 6-	Si no existe tira una excepción.
         * 7-	Actualiza la clave en la base de datos.
         * 8-	Si la actualización fue exitosa retorna error=0 y el mensaje correspondiente.
		 *
		 * @param string $login: login del usuario
		 * @param string $password: password actual del usuario
         * @param string $nueva_pass: nueva password del usuario
         *
		 * @return array Arreglo con dos elementos "error" que indica si ocurrió un error y "mensaje" que retorna el mensaje de éxito de la actualización.	
		 **/
		public function modificarPassword($login = "", $password = "", $nueva_pass = ""){
			try{
				# Se fija si los parametros login, clave nueva y clave antigua no estan definidos y o son vacíos, si es asi tira una excepción
				if (!isset($login) or !isset($password) or !isset($nueva_pass) or ($login == "") or ($password == "") or ($nueva_pass == "")){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('009', array());
					throw new Exception($mensaje, '009');
				}
				# Obtiene desde el archivo de configuración el formato que tiene que cumplir la clave.
				$formato_pwd = $this->configuracion->getDato('formato_pwd');
				# Se fija si la nueva clave cumple el formato
				if(!preg_match('/' . $formato_pwd . '/', $nueva_pass)){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('017', array());
					throw new Exception($mensaje, '087');
				}
				# Calcula el SHA1 de la clave nueva y la clave vieja				
				$nueva_pass_cod = password_hash($nueva_pass, PASSWORD_DEFAULT);

				$consulta = ' SELECT * FROM usuario WHERE login = ?';
				$res = $this->basedatos->ExecuteQuery($consulta, array($login));
				# Si no existe tira una excepción
				if (!isset($res[0]->password)){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('001', array());
					throw new Exception($mensaje, '001');
				}
				
				if( !$this->__verificarPassword($login, $password, $res[0]->password) ){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('010', array());
					return array("error"=>1,"mensaje"=>$mensaje);
				}				
				
				# Actualiza la clave en la base de datos
				$consulta = ' UPDATE usuario SET password = ?,resetear = 0, fecha_cambio_password = NOW() WHERE login = ?';
				$res2 = $this->basedatos->ExecuteNonQuery($consulta, array($nueva_pass_cod, strtolower($login)));
				# Si la actualización fue exitosa retorna error=0 y el mensaje correspondiente
				if($res2==1){
					$mensaje = MensajesUsuario::getMensaje('011', array());
					return array("error"=>0,"mensaje"=>$mensaje);
				}else{
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('012', array());
					throw new Exception($mensaje, '097');
				}
			}
			catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		/** 
		 *  Genera un password para el usuario, se lo asigna, y lo retorna.
		 * 
		 *  La secuencia de ejecución es la siguiente:
         *  1-	Obtiene el Id del usuario.
         *  2-	Verifica que se ingrese el largo de la clave.
         *  3-	Genera una password randomica del largo especificado.
         *  4-	Sustituye la password en la base de datos y marca que el usuario debe resetearla.
		 *
		 *
		 * @param string $login: login del usuario
 		 * @param string $largo: largo del nuevo password
         *
		 * @return string password: nuevo password del usuario.	
		 **/
		public function resetearPasswd($login = "", $largo = ""){
			try{
				# Obtiene el Id del usuario
				$idUsuario = $this->__getIdUsuario(strtolower($login));
				if($idUsuario==0){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('004', array("USUARIO" => strtolower($login)));
					throw new Exception($mensaje, "004");
				}
				# Verifica que se ingrese el largo de la clave
				if (!isset($largo) or $largo == ""){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('013', array());
					throw new Exception($mensaje, '013');
				}
				# Genera una password randomica del largo especificado
				$password = $this->generarPwd($largo);
				$password_cod = password_hash($password, PASSWORD_DEFAULT);
				# Sustituye la password en la base de datos y marca que el usuario debe resetearla
				$consulta = 'UPDATE usuario SET password = ?, resetear = 1 WHERE id = ?';
				$res = $this->basedatos->ExecuteNonQuery($consulta, array($password_cod, $idUsuario));
				return $password;
			}
			catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		/** 
		 * Genera una password del largo ingresado en forma aleatoria.
		 * 
		 * La secuencia de ejecución es la siguiente:
		 * 	1-	Crea una password vacía.
		 * 	2-	Crea un array con todos los caracteres posibles para el password.
		 * 	3-	Calcula el índice máximo del array.
		 * 	4-	Repite largo veces el siguiente proceso. Obtiene aleatoriamente un caracter del array de opciones y lo concatena a la password.
		 * 	5-	Si no se logró una password del largo solicitado se tira una excepción.
		 * 	6-	Si se generó correctamente lo retorna.
		 *
		 * @param integer $largo: largo del password
		 *
		 * @return string password generada 
		 *
		**/
		public function generarPwd($largo){
			try{
				# Crea una password vacía
				$pasword = "";
				# Crea un array con todos los caracteres posibles para el password
				$opciones = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','2','3','4','5','6','7','8','9');
				# Calcula el indice maximo del array
				$max = count($opciones)-1;
				# Repite largo veces el siguiente proceso
				for($i=0; $i<$largo; $i++) {
					# Obtiene randomicamente un caracter del array de opciones y lo concatena a la password
					$posicion = rand(0,$max);
					$carcacter = $opciones[$posicion];
					$pasword .= $carcacter;
				}
				# Si no se logró una password del largo solicitado se tira una excepción
				if (strlen($pasword) != $largo){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('ADM_USR_011', array());
					throw new Exception($mensaje,'011');
				}
				# Si se generó correctamente lo retorna
				return $pasword;
			}catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}	
		}
		

		/**
		 * Verifica la password
		 * 
		 * Verifica si la contraseña pasada como parametro coincide con el hash, si coinciden retorna true,
		 * en caso contrario retorna false. Ademas si coinciden regenera el hash y lo actualiza en la base de datos,
		 * si la funcion password_needs_rehash indica que hay que regenerarlo
		 *
		 * @param string $login login del usuario
		 * @param string $password password del usuario
		 * @param string $hash hash resultado de la codificación de la password
		 *
		 * @return boolean resultado de la verificación
		 *
		 * @access private
		 **/
		protected function __verificarPassword($login ="", $password ="", $hash =""){
			try{
				// Verificar el hash almacenado con la contraseña en texto plano
				if (password_verify($password, $hash)) {
					// Comprobar hay un nuevo algoritmo de hash o ha cambiado el coste
					if (password_needs_rehash($hash, PASSWORD_DEFAULT)) {
						// Si es así, crear un nuevo hash y reemplazar el antiguo
						$newHash = password_hash($password, PASSWORD_DEFAULT);
						// Actualiza la contraseña en la base de datos
						$consulta = 'UPDATE usuario SET password = ?, WHERE login = ?';
						$res = $this->basedatos->ExecuteNonQuery($consulta, array($newHash, strtolower($login)));
						if($res==0){
							$this->error = 1;
							$mensaje = MensajesUsuario::getMensaje('012', array());
							throw new Exception($mensaje, '012');
						}
					}
					return true;
				}
				return false;
			}catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
				
		/** 
		 * Actualiza el password de un usuario.
		 * 
		 * La secuencia de ejecución es la siguiente:
		 *  1-	Si el login o la password son vacíos tira una excepción.
		 *  2-	Verifica que la password cumpla con el formato cargado desde el archivo de configuración.
		 *  3-	Calcula el SHA1 de la password.
		 *  4-	Verifica si existe el usuario con el login ingresado.
		 *  5-	Actualizo la clave en la base de datos.
		 *
		 * @param string $login: login del usuario
		 * @param string $nueva_pass: nueva password del usuario 
		 *
		 * @return array Arreglo con los siguientes elementos "error" que indica si ocurrió un error y “mensaje” que contiene el mensaje.	
		 *
		**/
		protected function __actualizarPassword($login = "",$nueva_pass = ""){
			try{
				# Si el login o la password son vacíos tira una excepción
				if (!isset($login) or !isset($nueva_pass) or ($login == "") or ($nueva_pass == "")){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('009', array());
					throw new Exception($mensaje, '009');
				}
				# Verifica que la password cumpla con el formato cargado desde el archivo de configuración
				$formato_pwd = $this->configuracion->getDato('formato_pwd');
				if(!preg_match('/' . $formato_pwd . '/', $nueva_pass)){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('017', array());
					return array("error"=>1,"mensaje"=>$mensaje);
				}
				# Calcula el SHA1 de la password
				//$nueva_pass = sha1($nueva_pass);
				$nueva_pass_cod = password_hash($nueva_pass, PASSWORD_DEFAULT);
				# Verifica si existe el usuario con el login ingresado
				$consulta = ' SELECT * FROM usuario WHERE login = ?';
				$res = $this->basedatos->ExecuteQuery($consulta, array(strtolower($login)));
				if (!isset($res[0]->id)){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('018', array("USUARIO"=>$login));
					throw new Exception($mensaje, '018');
				}				
				
				# Actualizo la clave en la base de datos
				$consulta = ' UPDATE usuario SET password = ?,resetear = 0, fecha_cambio_password = NOW() WHERE login = ?';
				$res2 = $this->basedatos->ExecuteNonQuery($consulta, array($nueva_pass_cod, strtolower($login)));
				if($res2==1){
					$mensaje = MensajesUsuario::getMensaje('011', array());
					return array("error"=>0,"mensaje"=>$mensaje);
				}else{
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('012', array());
					throw new Exception($mensaje, '097');
				}
			}catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		
		/**
		 * Retorna todos los datos de un usuario
		 *
		 * @param string $login es el login del usuario
		 *
		 * @return array Arreglo conteniendo todos los datos del usuario
		 *
		 **/
		public function getDatosUsuario($login){
			try{
				# Si el identificador del usuario es vacio o 0 tira una excepcion
				if($login == ""){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('019', array());
					throw new Exception($mensaje, '019');
				}
				
				$idUsuario = $this->__getIdUsuario(strtolower($login)); // Obtiene el id del Usuario
				if($idUsuario==0){
					$this->error = 1;
					$mensaje = MensajesUsuario::getMensaje('004', array("USUARIO" => strtolower($login)));
					throw new Exception($mensaje, "004");
				}
				
				$resetear = "";
				# Obtengo los datos generales del usuario
				$resUsuario = $this->getUsuario($login);
				if ($resUsuario['habilitado'] == 'S'){
					$resetear  = $resUsuario['resetear'];
				}
				# Creo una nueva sesion
				$this->cargarSesion(null);
				# Guardo nombre de usuario en sesion
				$this->session->set("usuario", strtolower($login));
				# Creo una clase para retornar el resultado
				$salida = new stdClass();
		
				$salida->resetear = $resetear;
				$salida->session  = $this->session->getIdSession();
				$salida->valido   = 1;
				$salida->id       = $idUsuario;
		
				$mensaje = MensajesUsuario::getMensaje('002', array());
				$salida->mensaje = $mensaje;
		
				return $salida;
			}
			catch(Exception $e){
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
			
	} // Fin de la clase