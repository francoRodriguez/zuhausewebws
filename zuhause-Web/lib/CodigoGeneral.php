<?php

namespace lib;

use \lib\nucleo\WSCliente;
use \Exception;

class CodigoGeneral {
	public static function logout(&$sesion) {
		try{
			$ws = new WSCliente ( Constantes::$WS_URL, Constantes::$WS_NAME_SPACE );
			$res = $ws->call ( "logout", array (
					"sesion" => $sesion->getSesionPerl () 
			) );
			if ($ws->isError ()) {
				error_log ( "CodigoError: " . print_r ( $ws->getCodigoError (), 1 ) );
				error_log ( "MensajeError: " . print_r ( $ws->getMensajeError (), 1 ) );
				$sesion->cerrar ();
				return ("Error");
			} else {
				$respuesta = $res->error;
				if ($respuesta == 0) {
					$sesion->logueado = false;
					$sesion->cerrar ();
					return ("ok");
				} else {
					$sesion->cerrar ();
					return ($respuesta);
				}
			}
		}catch(Exception $e){
			return ("Error");
		}
	}
	
	public static function get_datos_media($mobile) {
		$esMobile = 0;
		$esTablet = 0;
		$esPC = 0;
		// Obtenemos el tipo de dispositivo desde el cual se esta ingresando
		if ($mobile->isMobile ()) {
			// Esta accediendo desde un Telefono
			$esMobile = 1;
		} else {
			if ($mobile->isTablet ()) {
				// Esta accediendo desde una Tablet
				$esTablet = 1;
			} else {
				// Esta accediendo desde una PC
				$esPC = 1;
			}
		}
		// Obtenemos el SO del dispositivo
		if ($mobile->isiOS ()) {
			// El dispositivo es Apple
			$tipoDispositivo = "Apple";
		} else {
			if ($mobile->isAndroidOS ()) {
				// El dispositivo es Android
				$tipoDispositivo = "Android";
			} else {
				$tipoDispositivo = "Otro";
			}
		}
		if ($esPC == 1) {
			// Recogemos el user_agent del visitante
			$user_agent = $_SERVER ['HTTP_USER_AGENT'];
			$plataformas = array (
					'Windows 10' => 'Windows NT 10.0+',
					'Windows 8.1' => 'Windows NT 6.3+',
					'Windows 8' => 'Windows NT 6.2+',
					'Windows 7' => 'Windows NT 6.1+',
					'Windows Vista' => 'Windows NT 6.0+',
					'Windows XP' => 'Windows NT 5.1+',
					'Windows 2003' => 'Windows NT 5.2+',
					'Windows' => 'Windows otros',
					'iPhone' => 'iPhone',
					'iPad' => 'iPad',
					'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
					'Mac otros' => 'Macintosh',
					'Android' => 'Android',
					'BlackBerry' => 'BlackBerry',
					'Linux' => 'Linux' 
			);
			foreach ( $plataformas as $plataforma => $pattern ) {
				if (preg_match ( '/'.$pattern.'/', $user_agent ) && $encontro = 0) {
					$tipoDispositivo = $plataforma;
					$encontro = 1;
				}
			}
			if ($encontro == 0) {
				$tipoDispositivo = 'Otras';
			}
		}
		// Obtenemos el navegador del dispositivo
		if (strstr ( $mobile->getUserAgent (), "OPR" )) {
			// El navegador es Chrome
			$navegador = "Opera";
		} else {
			if (strstr ( $mobile->getUserAgent (), "Firefox" )) {
				// El navegador es Firefox
				$navegador = "Firefox";
			} else {
				if (strstr ( $mobile->getUserAgent (), "Chrome" )) {
					// El navegador es Opera
					$navegador = "Chrome";
				} else {
					$navegador = "Otro";
				}
			}
		}
		return array (
				"esPC" => $esPC,
				"esTablet" => $esTablet,
				"esMovil" => $esMobile,
				"tipoDispositivo" => $tipoDispositivo,
				"navegador" => $navegador 
		);
	}
	
	public static function getRealIP() {
		if (! empty ( $_SERVER ['HTTP_CLIENT_IP'] )) {
			return $_SERVER ['HTTP_CLIENT_IP'];
		}
		if (! empty ( $_SERVER ['HTTP_X_FORWARDED_FOR'] )) {
			return $_SERVER ['HTTP_X_FORWARDED_FOR'];
		}
		return $_SERVER ['REMOTE_ADDR'];
	}
	
	
	function getPlataforma() {
		// Recogemos el user_agent del visitante
		$user_agent = $_SERVER ['HTTP_USER_AGENT'];
		$plataformas = array (
				'Windows 10' => 'Windows NT 10.0+',
				'Windows 8.1' => 'Windows NT 6.3+',
				'Windows 8' => 'Windows NT 6.2+',
				'Windows 7' => 'Windows NT 6.1+',
				'Windows Vista' => 'Windows NT 6.0+',
				'Windows XP' => 'Windows NT 5.1+',
				'Windows 2003' => 'Windows NT 5.2+',
				'Windows' => 'Windows otros',
				'iPhone' => 'iPhone',
				'iPad' => 'iPad',
				'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
				'Mac otros' => 'Macintosh',
				'Android' => 'Android',
				'BlackBerry' => 'BlackBerry',
				'Linux' => 'Linux'
		);
		foreach ( $plataformas as $plataforma => $pattern ) {
			if (preg_match ( '/'.$pattern.'/', $user_agent ))
				return $plataforma;
		}
		return 'Otras';
	}
	/**
	 * Reemplaza todos los acentos por sus equivalentes sin ellos
	 *
	 * @param $string
	 *  string la cadena a sanear
	 *
	 * @return $string
	 *  string saneada
	 */
	public static function __convertString($string){
		//$string = trim($string);
		//$string = utf8_encode($string);
		$string = mb_convert_encoding($string, "UTF-8");
		/*
		$string = str_replace(
				array('\xc0','\xc1','\xc8','\xc9','\xcc','\xcd','\xd1','\xd2','\xd3','\xd9','\xda','\xe0','\xe1','\xe2','\xe8','\xe9','\xec','\xed','\xf1','\xf2','\xf3','\xf9','\xfa'),
				//array("ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½","ï¿½"),
				array("A","A","E","E","I","I","N","O","O","U","U","a","a","a","e","e","i","i","n","o","o","u","u"),
				$string);
		*/
		$string = str_replace(
				array("ÃƒÂ¡","ÃƒÂ©","Ãƒ*","ÃƒÂ³","ÃƒÂº","Ãƒ ","Ãƒâ€°","Ãƒ ","Ãƒâ€œ","ÃƒÅ¡","ÃƒÂ±","Ãƒâ€˜","Ã‚Âº","Ã‚Âª","Ã‚Â¿" ),
				array("Ã¡","Ã©","Ã­","Ã³","Ãº","Ã�","Ã‰","Ã�","Ã“","Ãš","Ã±","Ã‘","Âº","Âª","Â¿"),
				$string);
		
		return $string;
	
	}
	public static function objectToArray($data){
		if (is_array($data) || is_object($data)){
			$result = array();
			foreach ($data as $key => $value){
				$result[$key] = self::objectToArray($value);
			}
			return $result;
		}
		return $data;
	}
}
?>