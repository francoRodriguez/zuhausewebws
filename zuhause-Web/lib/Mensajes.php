<?php

namespace lib;

class Mensajes {
	private static $mensajes = array (
	    "001" => 	"Ocurri&oacute; un error.",
	    "002" => 	"Usted no se ha logueado.",
		"003" =>    "La nueva contrase&ntilde;a es inv&aacute;lida"		
			
	);
	
	public static function getMensaje($codigoMensaje = "", $params = array()) {
		$mensaje = self::$mensajes [$codigoMensaje];
		foreach ( $params as $k => $v ) {
			$mensaje = preg_replace ( "/#$k#/", $v, $mensaje );
		}
		return $mensaje;
	}
}
