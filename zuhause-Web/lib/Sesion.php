<?php

namespace lib;

use \lib\nucleo\PHPSesion;

define("EXP_LOGIN",'/^([a-zA-Z0-9@\_\.-]|[[:space:]]){3,15}$/' ); // quitar enies del login!!!!!!!!
define ( "EXP_DOCUMENTO", '/^(\w){6,15}$/' );
define("EXP_PWD",'/^([a-zA-Z0-9@\_\.-]|[[:space:]]){8,12}$/' );
define("EXP_EMAIL",'/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/');
define("LARGO_PWD", 6);
define("EXP_CUENTA", '/^([0-9]\/)?([0-9]+)$/');
define("LARGO_CUENTA", 10);
define("TOKEN_DURATION", 2700);
//define("COOKIE_TIME", 2700);

/*
 * Hereda de la clase PHPSesion, en esta clase se definen todos
 * los campos que tendra mi objeto sesion. 
 */
class Sesion extends PHPSesion {

	public $codigoError			=	"";
	public $mensajeError		=	"";
	public $SesionPerl 			= null;
	public $resetear			= null;
	public $csrf				= array();
	

	function Sesion ($nueva = false) {
		$this->PHPSesion($nueva);
	}
	
	function getSesionPerl(){
		return $this->SesionPerl;
	}
	function setSesionPerl($s){
		$this->SesionPerl=$s;
	}
	function getCodigoError(){
		return $this->codigoError;
	}
	function setCodigoError($var){
		$this->codigoError=$var;
	}
	function getMensajeError(){
		return $this->mensajeError;
	}
	function setMensajeError($var){
		$this->mensajeError=$var;
	}

	function getLogueado(){
		if (isset($this->logueado)){
			return $this->logueado;
		}
		return false;
	}
	public function salvar(){
		/*
		 * Extiendo el tiempo de la cookie con la hora actual mas 
		 * el tiempo seteado como preestablecido
		 * 
		 * */
		if(isset($_COOKIE[Constantes::$NOMBRE_SESION])){
			$cookie = $_COOKIE[Constantes::$NOMBRE_SESION];
			//setcookie (session_name(), session_id(), time() + COOKIE_TIME, '/');
			setcookie (session_name(), session_id(), time() + Constantes::$COOKIE_LIFETIME, '/');
		}
		parent::salvar();
	}
	/**
	 * Token para formularios
	 **/
	public function creaToken($idPagina) {
		$token 			= base64_encode(openssl_random_pseudo_bytes(32));
		$token 			= preg_replace("/([^A-Za-z0-9])/","", $token);//urlencode($token);
		$token_creation = time();
		//$this->csrf	= array($idPagina => array(	'token' 			=> $token, 
		//										'token_creation' 	=> $token_creation));
		$this->csrf[$idPagina]['token']			 = $token;
		$this->csrf[$idPagina]['token_creation'] = $token_creation;
		
		return $token;
	}
	
	public function compruebaTokenValido($idPagina, $tokenRecibido) {
		if (!isset($this->csrf[$idPagina])) {
			error_log("No se encuentra el token para la pagina en la sesion");
			return false;
        }
		if ($this->csrf[$idPagina]['token'] !== $tokenRecibido) {
			error_log("Los token son diferentes");
			return false;
		}
		if (time() - $this->csrf[$idPagina]['token_creation'] > TOKEN_DURATION) {
			error_log("Excede el TOKEN_DURATION");
			return false;
		}
		return true;
	}
	/** fin token **/
	function parsear_Error($tpl,$dir_relativa="") {
		$error="";
		if (isset($_GET['codigoError'])) {
	       	$error = $tpl->load($dir_relativa."recursos/error_tpl.html");
			$error = $tpl->replace( $error, array(
					"ERROR_CODIGO" =>$_GET['codigoError'],
					"ERROR_MENSAJE"=>$_GET['mensajeError'])
				 );
		}
		else
			if ($this != NULL){
				if ($this->getCodigoError()!= "")
				{
			       	$error = $tpl->load($dir_relativa."recursos/error_tpl.html");
			       	$error = $tpl->replace( $error, array(
							"ERROR_CODIGO" =>$this->getCodigoError(),
							"ERROR_MENSAJE"=>$this->getMensajeError())
					 		);
					$this->setCodigoError("");
			       	$this->setMensajeError("");
				}
			}

		return $error;
	}
	
	function getMenu($dir_relativa=""){
		//error_log("========================");
		//error_log(print_r($this->gruposUsr,1));
		//error_log("========================");
		
		$menu = $dir_relativa . "menu/menu-grupo-control.tpl";
		//if(array_key_exists("root", $this->grupos) or array_key_exists("administrador", $this->grupos)){
		if(isset($this->gruposUsr->{"root"}) or isset($this->gruposUsr->{"administrador"})){
			error_log("admin");
			$menu = $dir_relativa . "menu/menu-administrador.tpl";
		}
		elseif(isset($this->gruposUsr->{"gruposcontrol"})){
			error_log("grupos-control");
			$menu = $dir_relativa . "menu/menu-grupo-control.tpl";
		}
		elseif(isset($this->gruposUsr->{"call-center"})){
			error_log("call-center");
			$menu = $dir_relativa . "menu/menu-call-center.tpl";
		}
		error_log($menu);
		return $menu;
	}
	
}

function pwd_iguales($a){
	if($a["nueva_password"]!=$a["confirmacion"])
		return "V005";
	else
		return "";
}

function la_misma($a){
	if($a["password"]==$a["nueva_password"])
		return "V006";
	else
		return "";
}

?>