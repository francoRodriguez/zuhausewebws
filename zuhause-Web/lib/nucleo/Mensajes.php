<?php

namespace lib\nucleo;
/*
 * Lista de mensajes que puede ser desplegados al usuario final y/o archivos de log
 * Todos los mensajes desplegados por el cliente web deberan estar definidos en este archivo.
 *
 *	El array asociativo MENSAJES_LOG sera de la forma {Cddd=><mensaje>,....}
 *	El array asociativo MENSAJES_USR sera de la forma {Cddd=><mensaje>,....}
 *
 *	Donde Cddd sera un string, por ejemplo: "C001", "C123".
 *	La C indica que es un codigo de error del cliente web (capa presentacion) para diferenciarlo con los
 *	codigos de error del servidor (capa de logica) que seran de la forma Sddd.
 */

class Mensajes{
	/*
	public static $MENSAJE_LOG = array ( 	"C000" => "Funcionalidad en Construcci&oacute;n",
											"C001" => "No se pudo establecer la conexi&oacute;n con el servidor de aplicaci&oacute;n",
											"C002" => "No se pudo establecer la conexi&oacute;n con el servidor de aplicaci&oacute;n",
											"C003" => "Error en la comunicaci&oacute;n o invocaci&oacute;n al m&eacute;todo del web service",
											"C004" => "Error en la l&oacute;gica de invocaci&oacute;n al m&eacute;todo del web service",
											"C005" => "No se pudo iniciar la sesi&oacute;n en el web",
											"C006" => "Error al guardar la sesi&oacute;n en el web",
											"C007" => "No se pudo abrir la sesi&oacute;n en el web (posible TIMEOUT o PHPSESSIONID incorrecto)");


	public static $MENSAJE_USR = array (	"C000" => "Funcionalidad en Construcci&oacute;n",
											"C001" => "En este momento no se puede procesar la solicitud.<br />Vuelva a intentarlo en unos minutos.",
											"C002" => "En este momento no se puede procesar la solicitud.<br />Vuelva a intentarlo en unos minutos.",
											"C003" => "En este momento no se puede procesar la solicitud.<br />Vuelva a intentarlo en unos minutos.",
											"C004" => "En este momento no se puede procesar la solicitud.<br />Vuelva a intentarlo en unos minutos.",
											"C005" => "En este momento no se puede procesar la solicitud.<br />Vuelva a intentarlo en unos minutos.",
											"C006" => "En este momento no se puede procesar la solicitud.<br />Vuelva a intentarlo en unos minutos.",
											"C007" => "El tiempo de la sesion ha expirado, ingrese nuevamente",
											"C008" => "No existen muestras para los datos ingresados.",
											"V001" => "La confirmaci&oacute;n de la contrase&ntilde;a ingresada es inv&aacute;lida.",
											"V002" => "Contrase&ntilde;a actual incorrecta.",
											"V004" => "La nueva contrase&ntilde;a es inv&aacute;lida.",
											"V005" => "La nueva contrase&ntilde;a y confirmaci&oacute;n son distintas.",
											"V006" => "Nueva contrase&ntilde;a igual a la actual.",
											"V011" => "El usuario o la contrase&ntilde;a no es correcto.",
											"V012" => "Error al ingrsar el usuario.",
											"V013" => "No existen usuarios para los datos ingresados.",
											"V014" => "No tiene permisos para ejectar esta funcionalidad.",
											"V015" => "La nueva password del usuario <span id='usr_resetpwd'>*USR*</span> es <span id='pwd_resetpwd'>*PWD*</span>",
											"V016" => "Se elimin&oacute; el usuario <span id='usr_new'><strong>*USR*</strong></span> correctamente.",
											"V017" => "Se modific&oacute; la password correctamente.",
											"V018" => "Debe ingresa el login del usuario.",
											"V019" => "Se cre&oacute; el usuario <span id='usr_new'><strong>*USR*</strong></span> correctamente.<br>".
													  "La password del usuario es <span id='pwd_new'><strong>*PWD*</strong></span>",
											"V020" => "El login del usuario no tiene el formato adecuado.",
											"V021" => "No existen datos con la informaci&oacute;n ingresada.",
											"V022" => "Debe ingresar un n&uacute;mero de cuenta o la cuenta no tiene el formato adecuado ([n/nnnnnnnnn]n).",
											"V023" => "Debe ingresar al menos la fecha de inicio.",
											"V024" => "Formato de fecha incorrecto (aaaa-mm-dd).",
											"V025" => "Rango de fechas incorrecto.",
											"V026" => "Debe ingresar un n&uacute;mero de preimpreso o el preimpreso no tiene el formato adecuado (Preimpreso se compone de letras y d&iacute;gitos).",
											"V027" => "El a&ntilde;o no es correcto",
											"V028" => "El n&uacute;mero de orden no es correcto",
											"V029" => "El perfil de usuario no es correcto",
											"V030" => "La nueva password del usuario <span id='usr_new'><strong>*USR*</strong></span> es <span id='pwd_new'><strong>*PWD*</strong></span>");
	*/
	private static $mensajes = array(	"C000" => "Funcionalidad en Construcci&oacute;n",
										"C001" => "En este momento no se puede procesar la solicitud.<br />Vuelva a intentarlo en unos minutos.",
										"C002" => "En este momento no se puede procesar la solicitud.<br />Vuelva a intentarlo en unos minutos.",
										"C003" => "En este momento no se puede procesar la solicitud.<br />Vuelva a intentarlo en unos minutos.",
										"C004" => "En este momento no se puede procesar la solicitud.<br />Vuelva a intentarlo en unos minutos.",
										"C005" => "En este momento no se puede procesar la solicitud.<br />Vuelva a intentarlo en unos minutos.",
										"C006" => "En este momento no se puede procesar la solicitud.<br />Vuelva a intentarlo en unos minutos.",
										"C007" => "El tiempo de la sesion ha expirado, ingrese nuevamente",
										"C008" => "No existen muestras para los datos ingresados.",
										"V001" => "La confirmaci&oacute;n de la contrase&ntilde;a ingresada es inv&aacute;lida.",
										"V002" => "Contrase&ntilde;a actual incorrecta.",
										"V004" => "La nueva contrase&ntilde;a es inv&aacute;lida.",
										"V005" => "La nueva contrase&ntilde;a y confirmaci&oacute;n son distintas.",
										"V006" => "Nueva contrase&ntilde;a igual a la actual.",
										"V011" => "El usuario o la contrase&ntilde;a no es correcto.",
										"V012" => "Error al ingrsar el usuario.",
										"V013" => "No existen usuarios para los datos ingresados.",
										"V014" => "No tiene permisos para ejectar esta funcionalidad.",
										"V015" => "La nueva password del usuario <span id='usr_resetpwd'>*USR*</span> es <span id='pwd_resetpwd'>*PWD*</span>",
										"V016" => "Se elimin&oacute; el usuario <span id='usr_new'><strong>*USR*</strong></span> correctamente.",
										"V017" => "Se modific&oacute; la password correctamente.",
										"V018" => "Debe ingresa el login del usuario.",
										"V019" => "Se cre&oacute; el usuario <span id='usr_new'><strong>*USR*</strong></span> correctamente.<br>".
												  "La password del usuario es <span id='pwd_new'><strong>*PWD*</strong></span>",
										"V020" => "El login del usuario no tiene el formato adecuado.",
										"V021" => "No existen datos con la informaci&oacute;n ingresada.",
										"V022" => "Debe ingresar un n&uacute;mero de cuenta o la cuenta no tiene el formato adecuado ([n/nnnnnnnnn]n).",
										"V023" => "Debe ingresar al menos la fecha de inicio.",
										"V024" => "Formato de fecha incorrecto (aaaa-mm-dd).",
										"V025" => "Rango de fechas incorrecto.",
										"V026" => "Debe ingresar un n&uacute;mero de preimpreso o el preimpreso no tiene el formato adecuado (Preimpreso se compone de letras y d&iacute;gitos).",
										"V027" => "El a&ntilde;o no es correcto",
										"V028" => "El n&uacute;mero de orden no es correcto",
										"V029" => "El perfil de usuario no es correcto",
										"V030" => "La nueva password del usuario <span id='usr_new'><strong>*USR*</strong></span> es <span id='pwd_new'><strong>*PWD*</strong></span>");
	/*
	 *    Recibe un codigo de mensaje y un array de parametros
	 *    Sustituye cada elemento de parametros por el correspondiente de mensaje
	 */
	public static function getMensaje($codigoMensaje = "", $params = array()){
		//$mensaje = $this->mensajes[$codigoMensaje];
		$mensaje = self::$mensajes[$codigoMensaje];
		foreach($params as $k => $v){
			$mensaje = preg_replace("/#$k#/", $v, $mensaje);
		}
		return $mensaje;
	}
}
?>
