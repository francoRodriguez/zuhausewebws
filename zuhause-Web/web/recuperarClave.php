<?php
    include_once 'includes.php';      
        
    use lib\Constantes;
    use lib\Mensajes as Mensajes;
    use lib\ZuHauseWS;
        
    $hay_error = -1;
    $mensaje_error = "";
    
    if (isset ( $_POST["login"] ) ) {
        try {
            $login = $_POST["login"];
            $zh     = new zuHauseWS();
            $res = $zh->recuperarClave($login);
            error_log(print_r($res,1));
            
            if($res['valido'] == 1){     
                $hay_error = 0;
                // envio mail para cambiar la clave
                
            } else {
                // el mail ingresado no esta en la base
                $hay_error = 1;
            }           
            
            $mensaje_error = $res['mensaje'];
        } catch ( Exception $e ) {
            // / HACE ALGO EN CASO DE EXCEPCION
            $error = 1;
            $mensaje_error = Mensajes::getMensaje ( "022", array () );
            error_log ( $e->getMessage () );
        }
    
    }    
    echo $twig->render('recuperarClave/recuperarClave.html', [  'titulo'        => 'ZuHause - Recuperar Clave',
                                                                'anio'          => date('Y'),
                                                                'hay_error'     => $hay_error,
                                                                'mensaje_error' => $mensaje_error]);
                        
?>