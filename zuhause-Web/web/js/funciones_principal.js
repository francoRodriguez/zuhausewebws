function cargar_trabajo_seleccionado(eid, enombre){
	var parametros = {
	    "id_edificio" 	: eid,
	    "enombre"	 	: enombre,
    };
	$.ajax({
            data:  parametros,
            url:   ACTUALIZARTRABAJO_URL,
            type:  'post',		
            dataType: "json",
            beforeSend: function () {
            	//startLoader();
            },
            success: function(dato){				
            	if(dato.status == 'ok'){
            		window.location	= dato.pagina_redireccion;
	        	}else{
	        		if(dato.status == "-Sin-Sesion-"){
	            		window.location='index.php';
	           	  	}else{
	           	  		if(dato.status == "-Resetear-"){
	           			  window.location='cambiar_clave.php';
	           	  		}else{									
		                   	$('#mensajes').html("<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error: </strong>"+dato.mensajeError+"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");		                 
	           	  		}
	           	  	}
	        	}
            },
            error: function(dato, status, xhttpr){            	
				$('#mensajes').html("<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error: </strong>Ocurri&oacute; un error en cargar_trabajo_seleccionado() en funciones_principal.js. <strong>Contactar al administrador</strong>.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");				
    	    },
            complete: function(dato){
    			//stopLoader();
    	    }
    });
}