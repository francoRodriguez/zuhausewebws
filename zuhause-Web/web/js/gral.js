function startLoader(){
	$("#preloader").fadeIn("slow");
}

function stopLoader(){
	$("#preloader").fadeOut("slow");
}

function salvarSesion(){
	$.ajax({
        //data:  		JSON.stringify(parametros),
        url:   		URL_SALVAR_SESION,
        type:  		'post',
        dataType: 	'json',
        success: function ( dato ) {
        	if(dato.status == 'ok'){
        		console.log("sesion salvada");
        	}
        	else {
        		var params 		= new Object();
            	params.mensaje 	= dato.mensaje;
            	params.tipo		= 'error';
            	$("#error_ajax").load(URL_CARGAR_MENSAJE, JSON.stringify(params));
        	}
        },
        error: function(dato, status, xhttpr){
        	
	    },
	    complete: function(dato){
	    }
	});
}