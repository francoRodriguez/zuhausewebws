<?php
	include_once 'includes.php';

	
	use \lib\Constantes;
	use lib\ZuHauseWS;
	
	session_start();
	error_log(print_r($_SESSION,1));
	
	// Reviso que no se pase la sesion o si esta por cambiar clave
	if ( (!isset ($_SESSION['logueado']) ) ) {
	    if (isset($_SESSION['logueado']) or $_SESSION['logueado'] == 0) {
	        // last request was more than 30 minutes ago
	        session_unset();     // unset $_SESSION variable for the run-time
	        session_destroy();   // destroy session data in storage
	        header ( "Location: index.php" );
	        exit ();
	    }
	    if (isset ($_SESSION['resetear'] ) and $_SESSION['resetear'] == 1) {
	        header ( "Location: cambiar_clave.php" );
	        exit ();
	    }
	}
	   
	//voy a buscar los edificios para el usuario
	$zh     = new zuHauseWS();
	$res = $zh->getEdificios($_SESSION["idUsuario"]);
	error_log("principal:");
	error_log(print_r($res,1));
		
	$edificios 			= $res['edificios'];
	$cantidad_edificios = $res['cantidad_edificios'];

	
    $anio = date('Y');
	echo $twig->render('principal/principal.html', array(
											"titulo"		        => "Edificios",
											'anio'         			=> $anio,
	                                        'usuario' 				=> $_SESSION['nombreCompleto'],
	                                        'hay_usuario' 			=> $_SESSION['logueado'],
											"trabajo_nombre_actual"	=> "",
                                    	    "es_admin" 				=> $_SESSION['es_admin'],
                                    	    "es_propietario"		=> $_SESSION['es_propietario'],
                                    	    "es_inquilino"			=> $_SESSION['es_inquilino'],
	                                        'edificios'				=> $edificios,
											'cantidad_edificios'	=> $cantidad_edificios ));
?>											