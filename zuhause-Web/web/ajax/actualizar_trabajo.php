<?php
       
    session_start();
    
    $sal = new stdClass ();
    $sal->status = "ok";
    $sal->mensajeError = "Ocurri&oacute; un error.";
    
    if ( (!isset ($_SESSION['logueado']) ) ) {
        error_log("entra actualizar trabajo");
        if (isset($_SESSION['logueado']) or $_SESSION['logueado'] == 0) {
            // last request was more than 30 minutes ago
            $sal->status = "-Sin-Sesion-";
            $sal->mensajeError = "";
            echo json_encode ( $sal );
            exit ();
        }
        if (isset ($_SESSION['resetear'] ) and $_SESSION['resetear'] == 1) {
            $sal->status = "-Resetear-";
            $sal->mensajeError = "";
            echo json_encode ( $sal );
            exit ();
        }
    }
    
    if(isset($_POST["id_edificio"]) and isset($_POST["enombre"])){
        

    	$trabajo = $_POST["id_edificio"];
    	$nombre  = $_POST["enombre"];   	
    	
    	$_SESSION['trabajo_actual'] = $trabajo;
    	$_SESSION['trabajo_nombre_actual'] = $nombre;
    	
    	if($_SESSION['es_admin'] == 1){
    	    error_log("HAY QUE MODIFICAR LA DIRECCION DE ESADMIN EN ACTUALIZAR TRABAJO Y CAMBIAR A ACTUALIZAR EDIFICIO");
    		$sal->pagina_redireccion = "templates/misGastos/misGastos.html";
    	}else{
    	    if($_SESSION['es_propietario'] or $_SESSION['es_inquilino']){    	        
    			$sal->pagina_redireccion = "misGastos.php";
    		}else {    		    
    		    $sal->pagina_redireccion = "#!";
    		}
    	}
    }    
    
    echo json_encode( $sal );
