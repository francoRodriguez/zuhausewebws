<?php

/*
 * --------------------------------------*
 * INCLUDES *
 * --------------------------------------
 */

/* ------------- Incluyo el Autoloader de Twig --------------- */
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
//error_log( __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php');

$loader = new \Twig\Loader\FilesystemLoader(__DIR__. DIRECTORY_SEPARATOR . 'templates');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);
$twig = new \Twig\Environment($loader);

// cargo el Loader
include_once '..' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'nucleo' . DIRECTORY_SEPARATOR . 'Loader.php';	
include_once '..' . DIRECTORY_SEPARATOR . 'back' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'zuHauseWS.php';	
error_log('..' . DIRECTORY_SEPARATOR . 'back' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'zuHauseWS.php');