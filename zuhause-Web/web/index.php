<?php
    include_once 'includes.php';      
        
    use lib\Constantes;
    use lib\Mensajes as Mensajes;
    use lib\ZuHauseWS;
    

    session_start();
    //session_destroy(); si necesito cerrar sesion
    // si esta en una sesion puede que este aqui para cambiar clave. sino lo tiro a principal
    if ( (!isset ($_SESSION['logueado']) ) ) {

        if (isset ($_SESSION['resetear'] ) and $_SESSION['resetear'] == 1) {
            header ( "Location: cambiar_clave.php" );
            exit ();
        }
        if (isset ( $_SESSION['logueado'] ) and $_SESSION['logueado'] == 1) {
           
            header ( "Location: principal.php" );
            exit ();
        }else{
            $error = 1;
            $mensaje_error = Mensajes::getMensaje ( "002", array () );
        }        
    }
    
    //error_log(print_r($session),1);
    
    $hay_error = -1;
    $mensaje_error = "";
    
    if (isset ( $_POST["login"] ) and isset ($_POST ["password"] )) {
        try {
            $login = $_POST["login"];
            $clave = $_POST["password"];
            $zh     = new zuHauseWS();
            $res = $zh->ingresar($login, $clave);

            if($res['valido'] == 1){               
                //session_start();
                // initialize session variables
                $_SESSION['logueado']       = '1';
                $_SESSION['idUsuario']             = $res["idUsuario"];
                $_SESSION['nombreCompleto'] = $res["nombreCompleto"];
                $_SESSION['es_admin']       = $res["es_admin"];
                $_SESSION['es_inquilino']   = $res["es_inquilino"];
                $_SESSION['es_propietario'] = $res["es_propietario"];
                $_SESSION['resetear']       = 0;
                
                
                if ($_SESSION['resetear'] == 1) {
                    header ( "Location: cambiar_clave.php" );
                    exit ();
                } else {
                    if($_SESSION['es_admin'] == 1){
                        header ( "Location: principal.php" );
                        exit();
                    } else{
                        if($_SESSION['es_inquilino'] == 1){
                            header ( "Location: misGastos.php" );
                        } else { // es propietario
                            header ( "Location: gastosTotales.php" );
                        }                       
                        exit();
                    }
                    exit ();
                }
            } else {
                $hay_error = 1;
                $mensaje_error = $res['mensaje'];
            }
            
            
        } catch ( Exception $e ) {
            // / HACE ALGO EN CASO DE EXCEPCION
            $error = 1;
            $mensaje_error = Mensajes::getMensaje ( "022", array () );
            error_log ( $e->getMessage () );
        }
    }    
    $anio = date('Y');
    
    
    echo $twig->render('login/login.html', ['titulo'        => 'ZuHause - Login',
                                            'anio'          => $anio,
                                            'hay_error'     => $hay_error,
                                            'mensaje_error' => $mensaje_error]);
    
?>