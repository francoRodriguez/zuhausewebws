<?php
	include_once 'includes.php';

	
    use \lib\Constantes;    
	use lib\ZuHauseWS;
	
	session_start();	
	// Reviso que no se pase la sesion o si esta por cambiar clave
	if ( (!isset ($_SESSION['logueado']) ) ) {
	    if (isset($_SESSION['logueado']) or $_SESSION['logueado'] == 0) {
	        // last request was more than 30 minutes ago
	        session_unset();     // unset $_SESSION variable for the run-time
	        session_destroy();   // destroy session data in storage
	        header ( "Location: index.php" );
	        exit ();
	    }
	}
	   
	//voy a buscar los edificios para el usuario
    $zh     = new zuHauseWS();    
	$res = $zh->getGastosDelMes($_SESSION["idUsuario"], date('mY'));
	error_log("misGastos:");
    error_log(print_r($res,1));
    // Si no hay datos del mes mando un mensaje de warning
    if($res['valido'] != 1){
        $hay_error      = $res['valido']; //2 warning or 0 error
        $mensaje_error = $res['mensaje'];
    }

	
    $anio = date('Y');
	echo $twig->render('misGastos/misGastos.html', array(
											"titulo"		        => "Mis Gastos",
											'anio'         			=> $anio,
	                                        'usuario' 				=> $_SESSION['nombreCompleto'],
	                                        'hay_usuario' 			=> $_SESSION['logueado'],											
                                    	    "es_admin" 				=> $_SESSION['es_admin'],
                                    	    "es_propietario"		=> $_SESSION['es_propietario'],
                                            "es_inquilino"			=> $_SESSION['es_inquilino'],
                                            "mesAlfa"               => $res["mesAlfa"],
                                            "total"                 => $res["total"],
                                            "vto"                   => $res["vto"]));
?>											