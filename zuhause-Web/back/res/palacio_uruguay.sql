-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-08-2020 a las 22:41:55
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `palacio_uruguay`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edificio`
--

CREATE TABLE `edificio` (
  `eid` int(10) UNSIGNED NOT NULL,
  `enombre` varchar(255) NOT NULL DEFAULT '',
  `ecantidad_apartamentos` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `edireccion` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `edificio`
--

INSERT INTO `edificio` (`eid`, `enombre`, `ecantidad_apartamentos`, `edireccion`) VALUES
(1, 'Palacio Uruguay', 60, 'Av. Uruguay 1530'),
(2, 'Torre del Gaucho', 140, 'Av. 18 de Julio 1759');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `dia_alta` date NOT NULL,
  `habilitado` smallint(6) DEFAULT 1,
  `resetear` smallint(6) DEFAULT 1,
  `fecha_cambio_password` date NOT NULL,
  `ultimo_acceso` date DEFAULT NULL,
  `es_admin` smallint(6) NOT NULL DEFAULT 0,
  `es_inquilino` smallint(6) NOT NULL DEFAULT 0,
  `es_propietario` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `login`, `password`, `nombre`, `apellido`, `telefono`, `dia_alta`, `habilitado`, `resetear`, `fecha_cambio_password`, `ultimo_acceso`, `es_admin`, `es_inquilino`, `es_propietario`) VALUES
(1, 'rodriguezcfranco@gmail.com', 'root', 'Franco', 'Rodriguez', '091244893', '2020-08-21', 1, 0, '2020-08-21', '2020-08-21', 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_edificio`
--

CREATE TABLE `usuario_edificio` (
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `eid` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario_edificio`
--

INSERT INTO `usuario_edificio` (`uid`, `eid`) VALUES
(1, 1),
(1, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `edificio`
--
ALTER TABLE `edificio`
  ADD PRIMARY KEY (`eid`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `edificio`
--
ALTER TABLE `edificio`
  MODIFY `eid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
