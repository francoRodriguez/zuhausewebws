<?php
	namespace lib\administradorUsuario;
	
    class Mensajes{
       protected static $mensajes = array(							
		"000"=>"Debe introducir login y contrase&ntilde;a.",	
		"001"=>"Login o contrase&ntilde;a incorrectos.",
		"002"=>"Usuario cargado correctamente.",
		"003"=>"Parametro '#PARAMETRO#' invalido.",
		"004"=>"El usuario ingresado no existe",
		"005"=>"Parametro 'habilitado' invalido.",
		"006"=>"El login del usuario no puede ser nulo.",
		"007"=>"El usuario root no puede ser eliminado.",		
		"009"=>"Los parametros no pueden ser vacios.",
		"010"=>"La contrase&ntilde;a actual ingresada no es correcta",
		"011"=>"La contrase&ntilde;a fue actualizada correctamente",
		"012"=>"Error, no se pudo actualizar la contrase&ntilde;a",
		"013"=>"Debe especificar un largo.",
		"014"=>"Error, las contrase&ntilde;as son diferentes",
		"015"=>"Error, parametros erroneos",
		"016"=>"Error en obtener id de usuario, no hay usuarios con ese c&oacute;digo",
		"017"=>"La contrase&ntilde;a no cumple con el formato.",
		"018"=>"No existe el usuario #USUARIO#",
		"019"=>"Usuario inv&aacute;lido",
		"020"=>"Debe introducir login",
		"021"=>"Se envi&oacute; un mail para recuperar la clave a #USUARIO#",
	   );
	   
		public static function getMensaje($codigoMensaje = "", $params = array()){
			$mensaje = self::$mensajes[$codigoMensaje];
			foreach($params as $k => $v){
				$mensaje = preg_replace("/#$k#/", $v, $mensaje);
			}
			return $mensaje;
		}
    }