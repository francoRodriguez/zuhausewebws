<?php
	namespace lib; 
	use Exception;
	use stdClass;
	
	//define('PHP_TAB', "\t");
	
    class Configuracion{
	
        private $ruta_archivo 		= null;
        private $ambiente       	= null;
        private $datos          	= null;
		private $datos_todos    	= null;
        private $mensaje        	= null;
        private $ruta_ambiente 	 	= null;
        private $datos_separados	= null;
        
        public function __construct($archivo = "", $ambiente = "", $ruta_ambiente = ""){				
        	if($archivo == "" OR !file_exists($archivo)){
                error_log("ERROR :: ". __CLASS__ . " :: " . __METHOD__ ." line ". __LINE__);
            }
             
            $this->ruta_archivo = $archivo;            
            $this->ambiente     = $ambiente;
            $this->ruta_ambiente= $ruta_ambiente;
            
            $var_ambiente = @parse_ini_file ($this->ruta_ambiente, true);
            if($var_ambiente){
            	$this->ambiente = $var_ambiente["ambiente"];
            }
            
            $salida = array();
            $salida = parse_ini_file ($this->ruta_archivo, true);
            if(!$salida){
                error_log("ERROR :: ". __CLASS__ . " :: " . __METHOD__ ." line ". __LINE__);
            }
          	
			$this->datos_todos 		= $salida;
            $this->datos 			= $salida[$this->ambiente];
            $this->datos_separados	= $this->separar();
            $this->parseAll(); // los datos se pueden navegar como objetos
            return $this->datos;
        }
        function getRutaArchivo(){
            return $this->ruta_archivo;
        }
        function getDato($dato = ""){
        	if(!isset($this->datos[$dato])){
				error_log("ERROR :: ". __CLASS__ . " :: " . __METHOD__ ." line ". __LINE__);
			}
			return $this->datos[$dato];
        }
        /*
         * $arr_datos es un hash con el siguiente formato [dato => valor]
         */
        function setDato($ambiente = "desarrollo", $arr_datos){
            $fp = fopen($this->ruta_archivo, 'w');
            if(!$fp){
                error_log("ERROR :: ". __CLASS__ . " :: " . __METHOD__ ." line ". __LINE__);
            }
			foreach ($arr_datos as $k => $v){
            	$this->datos_todos[$ambiente][$k] = $v;
			}
			
			$str = "";
			foreach($this->datos_todos as $amb => $dat_n1){
				$str .= "[" . $amb . "]".PHP_EOL;
				foreach($dat_n1 as $k_n1 => $dat_n2){
					$str .= $k_n1 . " = " . $dat_n2 . PHP_EOL;
				}
				$str .= PHP_EOL;
			}
			fwrite($fp, $str);
            fclose($fp);
        }
        public function getAmbiente($ruta_ambiente = ""){
            return $this->ambiente;            
        }
        private function set_element(&$path, $data) {
        	return ($key = array_pop($path)) ? $this->set_element($path, array($key=>$data)) : $data;
        }
        private function separar(){
			$salida = array();
			foreach($this->datos as $k_n2 => $dat_n2){
				$porciones = explode(".", $k_n2);
				$arr = $this->set_element($porciones, $dat_n2);
				$arr = array_merge_recursive($salida, $arr);
				$salida = $arr;					
			}
			return $salida;
        }
        public function getDatosSeparados(){
        	return $this->datos_separados;
        }
        public function getDatos2Oject(){
        	return json_decode (json_encode ($this->datos_separados), FALSE);
        }
        private function parseAll(){
        
        	foreach( $this->datos as $k => $v) {
        		$campos = explode('.', $k);
        
        		$primero = array_shift($campos);
        
        		if(empty($this->{$primero})) {
        			$this->{$primero} = new stdClass();
        		}
        
        		if( count($campos) > 0 ){
        
        			$tmp = $this->{$primero};
        			$ultimo = null;
        
        			foreach ($campos as $key => $value) {
        
        				if(empty($tmp->{$value})) {
        					$tmp->{$value} = new stdClass();
        				}
        				$ultimo = $tmp;
        				$tmp = $tmp->{$value};
        
        			}
        			$ultimo->{$value} = $v;
        		}
        		else{
        			$this->{$primero} = $v;
        		}
        
        	}
        
        }
        
    }