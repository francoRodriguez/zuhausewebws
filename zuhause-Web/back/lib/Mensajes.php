<?php
    namespace lib;

    class Mensajes{
        private static $mensajes = array( 
            "001" => 	"Ocurrio un error en la funcion: #funcion#",
            "002" =>	"Debe introducir login y contrase&ntilde;a",
            "003" =>    "Ocurrio una excepcion en #funcion#, mensaje: #mensaje#",
            "004" =>	"Se produjo un error en el sistema, por favor vuelva a intentarlo",
            "005" =>	"Debe introducir login",
            "006" =>	"No se paso por parametro el Id del usuario",
        );

        public static function getMensaje($codigoMensaje = "", $params = array()){       	 
            $mensaje = self::$mensajes[$codigoMensaje];
            foreach($params as $k => $v){
                $mensaje = preg_replace("/#$k#/", $v, $mensaje);
            }
            return $mensaje;
        }
    }
