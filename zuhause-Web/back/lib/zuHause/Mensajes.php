<?php
    namespace lib\zuHause;

    class Mensajes{
        private static $mensajes = array( 
            "001" => 	"Se obtuvieron correctamente los registros",
            "002" =>	"Debe introducir login y contrase&ntilde;a",
            "003" =>    "Ocurrio una excepcion en #funcion#, mensaje: #mensaje#",
            "004" =>	"No se paso el idUsuario en la funcion: #funcion#",
            "005" =>	"Debe introducir login",
            "006" =>	"No se paso por parametro el Id del usuario",
            "007" =>    "No hay datos disponibles por el momento del mes"
        );

        public static function getMensaje($codigoMensaje = "", $params = array()){       	 
            $mensaje = self::$mensajes[$codigoMensaje];
            foreach($params as $k => $v){
                $mensaje = preg_replace("/#$k#/", $v, $mensaje);
            }
            return $mensaje;
        }
    }
