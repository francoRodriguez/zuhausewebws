<?php 

namespace lib\zuHause;	

//include "DataBase.php";
include "Mensajes.php";

use \lib\zuHause\Mensajes as MensajesZh;
use \lib\Database;
use \lib\Configuracion;	
use stdClass;
use Exception;
use lib\Mensajes;

    class zuHause{

		var $configuracion 	= null;
		var $basedatos		= null;
		var $session		= null;
		var $error 			= 0;
		protected $mail;
		
		
		/**
		* Constructor del AdmUsuario
		*
		* El constructor del AdmUsuario setea parámetros necesarios para el funcionamiento,
		* crea las clases 'Configuracion' y 'DataBase', inicializa una transacción en la base
		* y obtiene desde el archivo de configuración la cantidad de claves anteriores que se
		* deben verificar que no se repitan al momento de cambiar un clave
		*  
		* @param string $ruta_configuracion ruta del archivo desde donde se cargan las configuraciones
		* @param string $ambiente ambiente en el que se está corriendo el AdmUsuario (desarrollo o producción)
		* 
		* @access public
		*/
		public function __construct($ruta_configuracion = "", $ambiente = ""){			
			try{
				$this->ruta_configuracion 	= $ruta_configuracion;
				$this->ambiente		 		= $ambiente;
				$this->configuracion 		= new Configuracion($ruta_configuracion, $ambiente);			
				$this->basedatos 	 		= new Database($ruta_configuracion, $ambiente);				
				$this->error				= 0;				
				$this->basedatos->BeginTransaction();				
				//$this->historial_password = $this->configuracion->getDato("cant_historial_password");				
			}
			catch(Exception $e){
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
        }
        
		/**
		 * Obtenemos la informacion de los edificos que tiene asociada un usuario admin.
		 * esta info es la que se mostrara en principal al loguearse
		 */
		public function getEdificios($idUsuario){
			try{
				// Prepara la consulta para obtener los datos de la tabla usuario
				$consulta 	= 'select e.eid, e.enombre, e.ecantidad_apartamentos, e.edireccion from edificio e, usuario_edificio o where e.eid = o.eid and uid = ?';
				// esta consulta hacerla en una clase llamada zuHause mejor 
				$res 	  	= $this->basedatos->ExecuteQuery($consulta, array($idUsuario));

				$edificios = array();
				foreach($res as $row){
					$id        = $row->eid;
					$nombre     = $row->enombre;
					$direccion  = $row->edireccion;
					$cantAptos  = $row->ecantidad_apartamentos;
					$edificios[] = array(   "eid" => $id,
											"enombre"=> $nombre,
											"edireccion" =>$direccion,
											"ecantidadAptos"=>$cantAptos); 
				}
				//error_log("edificios:");
				//error_log(print_r($edificios,1));              
				$valido = 1;
				$mensaje = MensajesZh::getMensaje("001", array());
				$cantidad_edificios = count($edificios);
				return array("valido"=>$valido, "mensaje"=>$mensaje, "edificios"=>$edificios, "cantidad_edificios"=>$cantidad_edificios);
								
			}catch(Exception $e){
				$mensaje_excepcion = MensajesZh::getMensaje("003", array("funcion"=>"ingresar","mensaje"=>$e->getMessage()));
				error_log($mensaje_excepcion);
				return array("valido"=>0,"mensaje"=>$mensaje_excepcion);
			}
		}

		/**
		* Obtenemos la informacion de los gastos del mes actual
		* 
		* @param string $idUsuario id del usuario logueado
		* @param string $mesAnio mes y año acutal, 082020
		* 
		* @access public
		*/
		public function getGastosDelMes($idUsuario, $mesAnio){
			try{

				// Me quedo con el mes y el año por separado.
				$m = $this->_getMes($mesAnio);
				$mes = $m["mes"];
				$mesAlfa = $m["mesAlfa"];
				
				// Prepara la consulta para obtener los datos de la tabla usuario
				$consulta 	= 'select g.gtotal_a_pagar, g.gfecha_vto  from gastos g, emision e where g.gid = e.eid and e.emision = ?';
				// esta consulta hacerla en una clase llamada zuHause mejor 
				$res 	  	= $this->basedatos->ExecuteQuery($consulta, array($mesAnio));
				# Si no hay datos
				error_log(print_r($res,1));
				if(!isset($res[0])){
					return array("valido"=>0,"mensaje"=>MensajesZh::getMensaje("007", array()));
				}else{
					foreach($res as $row){
						$total_a_pagar = $row->gtotal_a_pagar;
						$fecha_vto = $row->gfecha_vto;	
					}
				
				}
				
				
				$valido = 1;
				$mensaje = MensajesZh::getMensaje("001", array());
				return array("valido"=>$valido, "mensaje"=>$mensaje,"mesAlfa"=>$mesAlfa, "total"=>$total_a_pagar, "vto"=>$fecha_vto);
								
			}catch(Exception $e){
				$mensaje_excepcion = MensajesZh::getMensaje("003", array("funcion"=>"ingresar","mensaje"=>$e->getMessage()));
				error_log($mensaje_excepcion);
				return array("valido"=>0,"mensaje"=>$mensaje_excepcion);
			}
		}









    		/**
		* Finalizar zuHause
		*
		* Se encarga de verificar si ocurrió algún error en la ejecución del AdmUsuario y si
		* ocurrió algún error le hace un rollback a la transacción activa de la base de datos
		* dejándola en un estado consistente, si no ocurrió ningún error commitea los cambios 
		* a la base de datos persistiendo de esta manera los datos.
		*
		* @access public
		*/
		public function finalizar(){ 
			# Se fija si ocurrió algún error
			if($this->error == 0){
				// Si no hubo error commitea
				$this->basedatos->CommitTransaction();
			}
			else{
				// Si hubo error hace rollback
				$this->basedatos->RollBackTransaction();
			}
		} 


		/* FUNCIONES PRIVADAS */

		/**
		* A partir de un mes anio devolvemos el mes numerico y alfa ejemplo 08 y Agosto en un array
		* 
		* @param string $mesAnio mes y año acutal, 082020
		* 
		* @access private
		*/


		private function _getMes($mesAnio){
			$patron = '/^(\d{2})\d{4}?/';
			$sustitución = '${1}';
			$mes =  preg_replace($patron, $sustitución, $mesAnio);

			switch ($mes) {
				case "01":
					$mesAlfa = "Enero";
					break;
				case "02":
					$mesAlfa =  "Febrero";
					break;
				case "03":
					$mesAlfa =  "Marzo";
					break;		
				case "04":
					$mesAlfa =  "Abril";
					break;		
				case "05":
					$mesAlfa =  "Mayo";
					break;		
				case "06":
					$mesAlfa =  "Junio";
					break;
				case "07":
					$mesAlfa =  "Julio";
					break;
				case "08":
					$mesAlfa =  "Agosto";
					break;
				case "09":
					$mesAlfa =  "Setiembre";
					break;
				case "10":
					$mesAlfa =  "Octubre";
					break;
				case "11":
					$mesAlfa =  "Noviembre";
					break;
				case "12":
					$mesAlfa =  "Diciembre";
					break;
			}

			return array("mes"=>$mes, "mesAlfa"=>$mesAlfa);

		}
    }

?>