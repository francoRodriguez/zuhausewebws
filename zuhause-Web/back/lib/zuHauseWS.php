<?php

namespace lib;

require "configuracion.php";
require "administradorUsuario/administradorUsuario.php";
require "logs.php";
require "zuHause/zuHause.php";

use \lib\zuHause\zuHause;
use \lib\administradorUsuario\AdmUsuario;
use \lib\Configuracion;
use \lib\Logs;
use \lib\Mensajes;
use Exception;


class zuHauseWS{

    private $error                  = 0;
    private $ruta_configuracion     = '../etc/configuracion.ini';
    private $ruta_ambiente          = 'etc/ambiente.ini';
    private $ambiente               = 'desarrollo';
    private $configuracion          = null;
    private $log                    = null;

    # Constructor de la clase
    public function __construct(){
        try {
            $this->configuracion    = new Configuracion($this->ruta_configuracion, $this->ambiente, $this->ruta_ambiente);            
            $this->ambiente         = $this->configuracion->getAmbiente();                                    
            $this->usuario          = new AdmUsuario($this->ruta_configuracion, $this->ambiente);
            $this->zh               = new zuHause($this->ruta_configuracion, $this->ambiente);
            //$this->log              = new Logs($this->ruta_configuracion, $this->ambiente);            
        } catch (Exception $e) {
            $mensaje_excepcion = Mensajes::getMensaje("001", array("funcion" => "__construct", "mensaje" => $e->getMessage()));
            error_log($mensaje_excepcion);    
            throw new Exception($e->getMessage(), (int)$e->getCode());        
        }
    }

    # Login de los usuarios al sistema
    public function ingresar($login = "", $clave = ""){
        try{
            
            if($login == "" or $clave == ""){
                $mensaje = Mensajes::getMensaje("002", array());
                return array("valido"=>0,"mensaje"=>$mensaje);
            }
            $res = $this->usuario->loguear($login, $clave);                
            $valido             = $res->valido;
            $mensaje            = $res->mensaje;
            if($valido == 1){
                $idUsuario          = $res->idUsuario;                    
                $nombreCompleto     = $res->nombreCompleto;
                $es_admin           = $res->es_admin;
                $es_inquilino       = $res->es_inquilino;
                $es_propietario     = $res->es_propietario;
                return array("valido"=>$valido,"mensaje"=>$mensaje, "nombreCompleto"=>$nombreCompleto, "es_admin"=>$es_admin, "es_inquilino"=>$es_inquilino, "es_propietario"=>$es_propietario, "idUsuario"=>$idUsuario);
            } else{
                return array('valido'=>$valido, "mensaje"=>$mensaje);
            }

            $this->usuario->finalizar();
                            
        }catch(Exception $e){
            $mensaje_excepcion = Mensajes::getMensaje("003", array("funcion"=>"ingresar","mensaje"=>$e->getMessage()));
            error_log($mensaje_excepcion);
            return array("valido"=>0,"mensaje"=>$mensaje_excepcion);
        }
    }

    
    # Recuperar clave
    /**
     * En esta funcion le pasamos un login y devolvemos valido o no,
     * si es valido enviamos un mail a la casillad e correo con el acceso a reiniciar contraseña
     */
    public function recuperarClave($login = ""){
        try{
            if($login == ""){
                $mensaje = Mensajes::getMensaje("005", array());
                return array("valido"=>0,"mensaje"=>$mensaje);
            }
            $res = $this->usuario->existeLogin($login);
            $valido             = $res->valido;
            $mensaje            = $res->mensaje;
            if($valido == 1){                    
                $this->__enviarMail($login);    
            }            
            $this->usuario->finalizar();
            return array("valido"=>$valido, "mensaje"=>$mensaje);
                            
        }catch(Exception $e){
            $mensaje_excepcion = Mensajes::getMensaje("003", array("funcion"=>"ingresar","mensaje"=>$e->getMessage()));
            error_log($mensaje_excepcion);
            return array("valido"=>0,"mensaje"=>$mensaje_excepcion);
        }
    }
    
    public function getEdificios($idUsuario = 0){
        if($idUsuario==0){
            $this->error = 1;
            $mensaje = MensajesUsuario::getMensaje('006', array());
            throw new Exception($mensaje, "006");
        }
        $res = $this->zh->getEdificios($idUsuario);  
        $this->zh->finalizar();      

        return $res;
    }

    public function getGastosDelMes($idUsuario = 0, $mesAnio){
        if($idUsuario==0){
            $this->error = 1;
            $mensaje = MensajesUsuario::getMensaje('006', array());
            throw new Exception($mensaje, "006");
        }

        $res = $this->zh->getGastosDelMes($idUsuario, $mesAnio);
        $this->zh->finalizar();

        return $res;
    }








    private function __enviarMail($login){            
        $to = "rodriguezcfranco@gmail.com";
        $subject = "Recuperar Clave";
        $body = "Hi,nn This is test email send by PHP Script";
        $headers = "From: sender\'s email";    

        /* Comento esta funcion por el momento hasta no tener bien el smtp de goolge al menos
        if (mail($to,$subject,$body,$headers)){
            error_log( "Correo enviado satisfactoriamente");
        }
        */
    }
}
