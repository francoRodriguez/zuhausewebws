<?php
	namespace lib; 
	
	use lib\Configuracion;
	use lib\Database;
	use Exception;
	
	class Logs{
		private $configuracion 		= null;
		private static $modo_debug	= 0;
		private $error				= 0; 
		
		public function __construct($ruta_configuracion = "", $ambiente = "desarrollo"){
			try{
				if($ruta_configuracion == "" or $ambiente == ""){
					throw new Exception("ERROR :: ". __CLASS__ . " :: " . __METHOD__ ." line ". __LINE__, 200);
				}
				
				$this->ruta_configuracion 	= $ruta_configuracion; 
				$this->ambiente				= $ambiente;
				$this->configuracion 		= new Configuracion ( $this->ruta_configuracion, $this->ambiente );
				$this->basedatos			= new Database($this->ruta_configuracion, $this->ambiente);
				$this->basedatos->BeginTransaction();
				$this->error = 0;
				self::$modo_debug = $this->configuracion->getDato ( "debug" );
				
			}catch(Exception $e){
				throw new Exception( $e->getMessage() , (int)$e->getCode() );
			}
		}
		
		public function finalizar(){
			try{
				# Se fija si ocurrió algún error
				if($this->error == 0){
					// Si no hubo error commitea
					$this->basedatos->CommitTransaction();
				}
				else{
					// Si hubo error hace rollback
					$this->basedatos->RollBackTransaction();
				}
			}catch(Exception $e){
				error_log($e->getMessage());
			}
		}
		public static function error_log( $mensaje = "", $prefijo = "" ){
			if(self::$modo_debug){
				
				if(!is_string($mensaje)) {
					if( !empty( $prefjo ) ) {
						error_log($prefijo . ": ");
					}
					error_log(print_r($mensaje,1));
				}
				else{
					error_log(print_r($prefijo . ": " . $mensaje, 1));
				}
				
			}
		}
		public function registrarLog($tipoLog = "", $datos = array()){
			try{
				if( ($datos["usuario"] == "") || (($datos["resultado"] != 0)&&($datos["resultado"] != 1)) ){
					throw new Exception("ERROR :: ". __CLASS__ . " :: " . __METHOD__ ." line ". __LINE__, 200);
				}else{	
					if($datos["usuario"] != 'root'){
						if($datos["usuario"] != 'error'){
	        				$select = "select id from usuario where login = ?";
	        				$res = $this->basedatos->ExecuteQuery($select, array($datos["usuario"]));
	        				if(!isset($res[0]->id)){
	        					$this->finalizar();
	        					return false;
	        				}
	        				$usuario = $res[0]->id;
						}else{
							$usuario = 0;
						}
        				$datos["usuario"] = $usuario;
						if(($tipoLog=="autentificacion")||($tipoLog=="cambiar_clave")||($tipoLog=="recuperacion_clave")||($tipoLog=="modificacion_clave")||($tipoLog=="administracion")||($tipoLog=="consulta")||($tipoLog=="deshabilitacion_lotes")){
							$consulta 	= 'INSERT INTO log (logtipo_log, logusuario, logip, logesmovil, logestablet, logespc, logagente, logsistoper) VALUES (?,?,?,?,?,?,?,?)';
							$idLog		= $this->basedatos->ExecuteNonQuery($consulta, array($tipoLog, $usuario, $datos["ip"], $datos["esMovil"], $datos["esTablet"], $datos["esPC"], $datos["navegador"], $datos["tipoDispositivo"]),true);
							$funcion = 'log_'.$tipoLog;
							$this->$funcion($idLog, $datos);	
						}else{
							throw new Exception("ERROR :: ". __CLASS__ . " :: " . __METHOD__ ." line ". __LINE__, 200);
						}	
					}else{
						$this->finalizar();
					}
				}		
			}catch(Exception $e){
				$this->error = 1;
				$this->finalizar();
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		
		private function log_deshabilitacion_lotes($idLog = "", $datos = array()){
			try{
				$consulta = 'INSERT INTO log_deshabilitacion_lotes (logid, , accion, lotes, resultado, mensaje) VALUES (?,?,?,?,?,?)';
				$res = $this->basedatos->ExecuteNonQuery($consulta, array($idLog,$datos["usuario"], $datos["accion"], $datos["lotes"],$datos["resultado"],$datos["mensajes"]), false);
				$this->finalizar();
			}catch(Exception $e){
				error_log($e->getMessage());
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		private function log_autentificacion($idLog = "", $datos = array()){
			try{
				$consulta = 'INSERT INTO log_autentificacion (logid, usuario, resultado, mensaje) VALUES (?,?,?,?)';
				$res = $this->basedatos->ExecuteNonQuery($consulta, array($idLog,$datos["usuario"],$datos["resultado"],$datos["mensajes"]), false);
				$this->finalizar();
			}catch(Exception $e){
				error_log($e->getMessage());
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		
		private function log_modificacion_clave($idLog = "", $datos = array()){
			try{
				$consulta = 'INSERT INTO log_modificacion_clave (logid, usuario, resultado, mensaje) VALUES (?,?,?,?)';
				$res = $this->basedatos->ExecuteNonQuery($consulta, array($idLog, $datos["usuario"], $datos["resultado"], $datos["mensajes"]), false);
				$this->finalizar();
			}catch(Exception $e){
				error_log($e->getMessage());
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		
		private function log_cambiar_clave($idLog = "", $datos = array()){
			try{
				$consulta = 'INSERT INTO log_cambiar_clave (logid, usuario, resultado, mensaje) VALUES (?,?,?,?)';
				$res = $this->basedatos->ExecuteNonQuery($consulta, array($idLog, $datos["usuario"], $datos["resultado"], $datos["mensajes"]), false);
				$this->finalizar();
			}catch(Exception $e){
				error_log($e->getMessage());
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		
		private function log_recuperacion_clave($idLog = "", $datos = array()){
			try{
				$consulta 	= 'INSERT INTO log_recuperacion_clave (logid, usuario, resultado, mensaje) VALUES (?,?,?,?)';
				$res 		= $this->basedatos->ExecuteNonQuery($consulta, array($idLog, $datos["usuario"], $datos["resultado"], $datos["mensajes"]), false);
				$this->finalizar();
			}catch(Exception $e){
				error_log($e->getMessage());
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
		
		private function log_consulta($idLog = "", $datos = array()){
			try{
				$consulta = 'INSERT INTO log_consulta (logid, usuario, tipo, _consultado, emision, lote, resultado, mensaje) VALUES (?,?,?,?,?,?,?,?)';
				$res = $this->basedatos->ExecuteNonQuery($consulta, array($idLog, $datos["usuario"], $datos["tipo"], $datos[""], $datos["emision"], $datos["lote"], $datos["resultado"], $datos["mensajes"]), false);
				$this->finalizar();
			}catch(Exception $e){
				error_log($e->getMessage());
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}

		
		private function log_administracion($idLog = "", $datos = array()){
			try{
				$consulta = 'INSERT INTO log_administracion (logid, usuario, accion, resultado, mensaje) VALUES (?,?,?,?,?)';
				$res = $this->basedatos->ExecuteNonQuery($consulta, array($idLog, $datos["usuario"], $datos["accion"], $datos["resultado"], $datos["mensajes"]), false);
				$this->finalizar();
			}catch(Exception $e){
				error_log($e->getMessage());
				$this->error = 1;
				throw new Exception( $e->getMessage( ) , (int)$e->getCode( ) );
			}
		}
		
	}